CONTAINER_NAME=${CONTAINER_NAME}

EXISTING_CONTAINER=$(docker ps -a -q -f name=$CONTAINER_NAME)

if [ -n "$EXISTING_CONTAINER" ]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi
